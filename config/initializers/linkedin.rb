S3_CONFIG = YAML.load_file("#{Rails.root}/config/secret_keys.yml")[Rails.env]['linkdin']

LinkedIn.configure do |config|
  config.token = S3_CONFIG['api_key']
  config.secret = S3_CONFIG['secret_key']
  #   config.default_profile_fields = ['education', 'positions']
end