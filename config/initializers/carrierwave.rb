S3_CONFIG=YAML.load_file("#{Rails.root}/config/secret_keys.yml")[Rails.env]

CarrierWave.configure do |config|
  config.cache_dir = "#{Rails.root}/tmp/"
  config.storage = :fog
  config.permissions = 0666
  config.fog_credentials = {
      :provider               => 'AWS',
      :aws_access_key_id      => S3_CONFIG['s3']['app_id'],
      :aws_secret_access_key  => S3_CONFIG['s3']['secret_key'],
  }
  config.fog_directory  = Rails.application.class.parent_name.downcase
end
