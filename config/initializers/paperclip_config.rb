if ['development','test'].include? Rails.env
  Paperclip::Attachment.default_options.merge!({
      storage: :filesystem ,
      url: "/system/:class/:id/:style/:filename",
      path: ":rails_root/public/system/:class/:id/:style/:filename"
    })
else
  S3_CONFIG=YAML.load_file("#{Rails.root}/config/secret_keys.yml")[Rails.env]
  Paperclip::Attachment.default_options.merge!({
      storage:  :s3,
      s3_credentials: { :access_key_id => S3_CONFIG['s3']['app_id'], :secret_access_key => S3_CONFIG['s3']['secret_key']},
      bucket: Rails.application.class.parent_name.downcase,
      path: "/:class/:id/:style/:filename"
    })
end
