S3_CONFIG = YAML.load_file("#{Rails.root}/config/secret_keys.yml")[Rails.env]['twitter']

Twitter.configure do |config|
  config.consumer_key       = S3_CONFIG['consumer_key']
  config.consumer_secret    = S3_CONFIG['consumer_secret']
  config.oauth_token        = S3_CONFIG['oauth_token']
  config.oauth_token_secret = S3_CONFIG['oauth_token_secret']
end