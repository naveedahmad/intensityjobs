# RailsAdmin config file. Generated on February 28, 2013 02:17
# See github.com/sferik/rails_admin for more informations
require Rails.root.join('lib', 'rails_admin', 'rails_admin_feed_mapping.rb')
require Rails.root.join('lib', 'rails_admin', 'rails_admin_feed_parser.rb')
require Rails.root.join('lib', 'rails_admin', 'rails_admin_marked_as_approved.rb')
require Rails.root.join('lib', 'rails_admin', 'rails_admin_reporting.rb')


RailsAdmin.config do |config|

  config.actions do
    # root actions
    dashboard # mandatory

    # collection actions
    index # mandatory
    new
    export
    import

    # member actions
    show
    edit
    delete
    show_in_app

    # Set the custom action here
    feed_mapping do
      visible do
        bindings[:abstract_model].to_s == 'FeedSchema'
      end
    end

    feed_parser do
      visible do
        bindings[:abstract_model].to_s == 'FeedSchema'
      end
    end

    reporting do
      visible do
        bindings[:abstract_model].to_s == 'Reporting::Report'
      end
    end

    marked_as_approved do
      visible do
        bindings[:abstract_model].to_s == 'Job'
      end
    end
  end

  ################  Global configuration  ################

  # Set the admin name here (optional second array element will appear in red). For example:
  config.main_app_name = ['Intensity Jobs', 'Admin']
  # or for a more dynamic name:
  # config.main_app_name = Proc.new { |controller| [Rails.application.engine_name.titleize, controller.params['action'].titleize] }

  # RailsAdmin may need a way to know who the current user is]
  config.current_user_method { current_admin } # auto-generated

  # If you want to track changes on your models:
  # config.audit_with :history, 'User'

  # Or with a PaperTrail: (you need to install it first)
  # config.audit_with :paper_trail, 'User'

  # Display empty fields in show views:
  # config.compact_show_view = false

  # Number of default rows per-page:
  # config.default_items_per_page = 20

  # Exclude specific models (keep the others):
  # config.excluded_models = ['Admin', 'FeedProvider', 'FeedSchema', 'FeedUrl', 'Job', 'User']

  # Include specific models (exclude the others):
  # config.included_models = ['Admin', 'FeedProvider', 'FeedSchema', 'FeedUrl', 'Job', 'User']

  # Label methods for model instances:
  # config.label_methods << :description # Default is [:name, :title]


  ################  Model configuration  ################

  # Each model configuration can alternatively:
  #   - stay here in a `config.model 'ModelName' do ... end` block
  #   - go in the model definition file in a `rails_admin do ... end` block

  # This is your choice to make:
  #   - This initializer is loaded once at startup (modifications will show up when restarting the application) but all RailsAdmin configuration would stay in one place.
  #   - Models are reloaded at each request in development mode (when modified), which may smooth your RailsAdmin development workflow.


  # Now you probably need to tour the wiki a bit: https://github.com/sferik/rails_admin/wiki
  # Anyway, here is how RailsAdmin saw your application's models when you ran the initializer:

  ###  FeedProvider  ###
  config.model 'Report' do
  end

  config.model 'FeedProvider' do

    #   # You can copy this to a 'rails_admin do ... end' block inside your feed_provider.rb model definition

    #   # Found associations:

    #     configure :jobs, :has_many_association
    #configure :feed_urls, :has_many_association

    #   # Found columns:

    #     configure :id, :integer
    #     configure :name, :string
    #     configure :website, :string
    #     configure :description, :string
    #     configure :active, :boolean
    #     configure :last_feed_processed_at, :datetime
    #     configure :total_feed_processed, :integer
    #     configure :is_processing_feed, :boolean
    #     configure :processing_feed_url_id, :integer
    #     configure :created_at, :datetime
    #     configure :updated_at, :datetime

    #   # Cross-section configuration:

    #     # object_label_method :name     # Name of the method called for pretty printing an *instance* of ModelName
    #     # label 'My model'              # Name of ModelName (smartly defaults to ActiveRecord's I18n API)
    #     # label_plural 'My models'      # Same, plural
    #     # weight 0                      # Navigation priority. Bigger is higher.
    #     # parent OtherModel             # Set parent model for navigation. MyModel will be nested below. OtherModel will be on first position of the dropdown
    #     # navigation_label              # Sets dropdown entry's name in navigation. Only for parents!

    #   # Section specific configuration:

    list do
      #filters [:id, :name, :active, :total_feed_processed] # Array of field names which filters should be shown by default in the table header
      #items_per_page 25 # Override default_items_per_page
                                                           #       # sort_by :id           # Sort column (default is primary key)
      #sort_reverse true # Sort direction (default is true for primary key, last created first)
      #exclude_fields :description
    end
    #     show do; end
   edit do
      exclude_fields :jobs_count, :parsed_feeds_count, :is_processing_feed, :processing_feed_url_id
    end
    #     export do; end
    #     # also see the create, update, modal and nested sections, which override edit in specific cases (resp. when creating, updating, modifying from another model in a popup modal or modifying from another model nested form)
    #     # you can override a cross-section field configuration in any section with the same syntax `configure :field_name do ... end`
    #     # using `field` instead of `configure` will exclude all other fields and force the ordering
  end


  ###  FeedSchema  ###

  config.model 'FeedSchema' do

    #   # You can copy this to a 'rails_admin do ... end' block inside your feed_schema.rb model definition

    #   # Found associations:

    configure :feed_urls, :has_many_association

    #   # Found columns:

    #     configure :id, :integer
    #     configure :name, :string
    #     configure :job_node, :string
    #     configure :job_node_wrapped_in_array, :boolean
    #     configure :attributes_map, :serialized
    #     configure :created_at, :datetime
    #     configure :updated_at, :datetime

    #   # Cross-section configuration:

    #     # object_label_method :name     # Name of the method called for pretty printing an *instance* of ModelName
    #     # label 'My model'              # Name of ModelName (smartly defaults to ActiveRecord's I18n API)
    #     # label_plural 'My models'      # Same, plural
    #     # weight 0                      # Navigation priority. Bigger is higher.
    #     # parent OtherModel             # Set parent model for navigation. MyModel will be nested below. OtherModel will be on first position of the dropdown
    #     # navigation_label              # Sets dropdown entry's name in navigation. Only for parents!

    #   # Section specific configuration:

    list do
      filters [:id, :name] # Array of field names which filters should be shown by default in the table header
      items_per_page 25 # Override default_items_per_page
      exclude_fields :job_node_wrapped_in_array
      #       # sort_by :id           # Sort column (default is primary key)
      #       # sort_reverse true     # Sort direction (default is true for primary key, last created first)
    end
    #     show do; end
    edit do
      exclude_fields :id, :job_node, :job_node_wrapped_in_array
    end
    #     export do; end
    #     # also see the create, update, modal and nested sections, which override edit in specific cases (resp. when creating, updating, modifying from another model in a popup modal or modifying from another model nested form)
    #     # you can override a cross-section field configuration in any section with the same syntax `configure :field_name do ... end`
    #     # using `field` instead of `configure` will exclude all other fields and force the ordering
  end


  ###  FeedUrl  ###

   config.model 'FeedUrl' do

  #   # You can copy this to a 'rails_admin do ... end' block inside your feed_url.rb model definition

  #   # Found associations:

  #     configure :feed_schema, :belongs_to_association 
 # configure :feed_provider, :belongs_to_association

  #   # Found columns:

  #     configure :id, :integer 
  #     configure :feed_schema_id, :integer         # Hidden 
  #     configure :feed_provider_id, :integer         # Hidden 
  #     configure :active, :boolean 
  #     configure :url, :string 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime 

  #   # Cross-section configuration:

  #     # object_label_method :name     # Name of the method called for pretty printing an *instance* of ModelName
  #     # label 'My model'              # Name of ModelName (smartly defaults to ActiveRecord's I18n API)
  #     # label_plural 'My models'      # Same, plural
  #     # weight 0                      # Navigation priority. Bigger is higher.
  #     # parent OtherModel             # Set parent model for navigation. MyModel will be nested below. OtherModel will be on first position of the dropdown
  #     # navigation_label              # Sets dropdown entry's name in navigation. Only for parents!

  #   # Section specific configuration:

  #     list do
  #       # filters [:id, :name]  # Array of field names which filters should be shown by default in the table header
  #       # items_per_page 100    # Override default_items_per_page
  #       # sort_by :id           # Sort column (default is primary key)
  #       # sort_reverse true     # Sort direction (default is true for primary key, last created first)
  #     end
  #     show do; end
         edit do
           exclude_fields :processing_feed, :jobs_parsed_count, :feed_parsed_count
         end
  #     export do; end
  #     # also see the create, update, modal and nested sections, which override edit in specific cases (resp. when creating, updating, modifying from another model in a popup modal or modifying from another model nested form)
  #     # you can override a cross-section field configuration in any section with the same syntax `configure :field_name do ... end`
  #     # using `field` instead of `configure` will exclude all other fields and force the ordering
 end


  ###  Job  ###

  # config.model 'Job' do

  #   # You can copy this to a 'rails_admin do ... end' block inside your job.rb model definition

  #   # Found associations:


  #   # Found columns:

  #     configure :id, :integer 
  #     configure :identifier, :string 
  #     configure :title, :string 
  #     configure :token, :string 
  #     configure :url, :string 
  #     configure :description, :text 
  #     configure :active, :boolean 
  #     configure :visible, :boolean 
  #     configure :feed_provider_id, :integer 
  #     configure :referenced_site_count, :integer 
  #     configure :created_at, :datetime 
  #     configure :updated_at, :datetime 

  #   # Cross-section configuration:

  #     # object_label_method :name     # Name of the method called for pretty printing an *instance* of ModelName
  #     # label 'My model'              # Name of ModelName (smartly defaults to ActiveRecord's I18n API)
  #     # label_plural 'My models'      # Same, plural
  #     # weight 0                      # Navigation priority. Bigger is higher.
  #     # parent OtherModel             # Set parent model for navigation. MyModel will be nested below. OtherModel will be on first position of the dropdown
  #     # navigation_label              # Sets dropdown entry's name in navigation. Only for parents!

  #   # Section specific configuration:

  #     list do
  #       # filters [:id, :name]  # Array of field names which filters should be shown by default in the table header
  #       # items_per_page 100    # Override default_items_per_page
  #       # sort_by :id           # Sort column (default is primary key)
  #       # sort_reverse true     # Sort direction (default is true for primary key, last created first)
  #     end
  #     show do; end
  #     edit do; end
  #     export do; end
  #     # also see the create, update, modal and nested sections, which override edit in specific cases (resp. when creating, updating, modifying from another model in a popup modal or modifying from another model nested form)
  #     # you can override a cross-section field configuration in any section with the same syntax `configure :field_name do ... end`
  #     # using `field` instead of `configure` will exclude all other fields and force the ordering
  # end
end
