namespace :jobs do
  desc "Scrap jobs from active FeedUrl"
  task :run_parser => :environment do
    FeedUrl.active.each do |feed_url|
      next if feed_url.ran_today?
      begin
        feed_url.feed_parsing_started
        parser = JobFeedParser.new feed_url
        parser.process_feed!
        feed_url.feed_parsing_finished
      rescue Exception => e
        feed_url.feed_parsing_finished
        AirbrakeLogger.log "Error running parser job", "FeedParser", feed_url: feed_url.id, exception: e, message: e.message, backtrace: e.backtrace
      end
    end

    `bundle exec rake sitemap:refresh`
  end


  desc "Remove two week old jobs"
  task :cleanup_old_job => :environment do
    Job.where("update_at < ? ", 3.week.ago).delete_all
  end
end
