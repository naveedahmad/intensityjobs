class AppConfig
  include Singleton

  cattr_reader :config
  class << self
    def get(path_to_key)
      value = load_config
      path_to_key.split('.').each do |key|
        value = value[key]
      end

      value
    rescue
      nil
    end

    protected

    def load_config
      @@config ||= YAML.load_file("#{Rails.root}/config/secret_keys.yml")[Rails.env]
    end
  end
end