module Utils
  class TokenGenerator
    def self.get_uniq_token(model, token_column_name = :token)
      raise "#{token_column_name} is not attribute of #{model}" unless model.column_names.include? token_column_name.to_s
      loop do
        token = SecureRandom.hex(16)
        break token unless model.where(token_column_name => token ).present?
      end
    end
  end
end
