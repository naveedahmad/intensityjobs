class AirbrakeLogger
  @@logger = Logger.new("#{Rails.root}/log/mp_airbrake_logs.log")

  def initialize(logger_file=nil)
    @logger = logger_file ? Logger.new("#{Rails.root}/log/#{logger_file}.log") : @@logger
  end

  def info(msg,error_class= 'MpAirbrakeLogger',parameters={})
    parameters[:logger] =  @logger unless parameters[:logger] 
    MpAirbrakeLogger.info msg,error_class,parameters
  end

  def log(msg,error_class = 'MpAirbrakeLogger',parameters={})
    parameters[:logger] =  @logger unless parameters[:logger] 
    MpAirbrakeLogger.log msg,error_class,parameters
  end

  def self.info(msg,error_class = nil, parameters={})
    logger = (parameters.delete :logger) || @@logger
    logger = Logger.new(logger) if logger.is_a? String 
    logger.info "Message:  #{msg}\n class: #{error_class} \n parameter:  #{parameters}"
  end

  def self.log(msg, error_class = 'AirbrakeLogger', parameters = {})
    parameters.merge!({:environment => Rails.env.to_s})

    #https://github.com/airbrake/airbrake#going-beyond-exceptions
    _params={}

    parameters.keys.each {|k| _params[k] = parameters[k].pretty_inspect}
    if Rails.env.development? || Rails.env.test?
      info msg,error_class,_params
    else
      parameters.delete :logger
      Airbrake.notify(
        :error_class   => error_class,
        :error_message => "#{msg}(self generated alert)",
        :parameters    => _params
      )
    end
  end

end
