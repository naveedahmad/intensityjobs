#
# Credit; http://fernandomarcelo.com/2012/05/rails-admin-creating-a-custom-action/
#

require 'rails_admin/config/actions'
require 'rails_admin/config/actions/base'

module RailsAdminMarkedAsApproved
end

module RailsAdmin
  module Config
    module Actions
      class MarkedAsApproved < RailsAdmin::Config::Actions::Base
        RailsAdmin::Config::Actions.register(self)
        # There are several options that you can set here.
        # Check https://github.com/sferik/rails_admin/blob/master/lib/rails_admin/config/actions/base.rb for more info.

        register_instance_option :collection do
          true
        end

        register_instance_option :bulkable? do
          true
        end

        register_instance_option :template_name do
          "marked_as_approved"
        end

        register_instance_option :http_methods do
          [:get, :put]
        end

        register_instance_option :link_icon do
          'icon-arrow-left'
        end

        register_instance_option :controller do

          Proc.new do
            if request.get?
              @objects = Job.filter params
              render :action => @action.template_name
            elsif request.post?
              @objects = list_entries(@model_config)
              @objects.update_all approved: true
              index
            end
          end
        end
      end
    end
  end
end
