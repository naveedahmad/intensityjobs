#
# Credit; http://fernandomarcelo.com/2012/05/rails-admin-creating-a-custom-action/
#

require 'rails_admin/config/actions'
require 'rails_admin/config/actions/base'

module RailsAdminFeedParser
end

module RailsAdmin
  module Config
    module Actions
      class FeedParser < RailsAdmin::Config::Actions::Base
        RailsAdmin::Config::Actions.register(self)
        # There are several options that you can set here.
        # Check https://github.com/sferik/rails_admin/blob/master/lib/rails_admin/config/actions/base.rb for more info.

        register_instance_option :member? do
          true
        end

        register_instance_option :http_methods do
          [:get, :put]
        end

        register_instance_option :link_icon do
          'icon-asterisk icon-spin'
        end

        register_instance_option :controller do
          Proc.new do
            if request.put?
              @parser_feeds = []
              feed_urls = FeedUrl.where id: params[:bulk_ids]

              if feed_urls.present?
                feed_urls.each do |feed_url|
                  parser = JobFeedParser.new feed_url
                  @parser_feeds << parser.process_feed!
                end
                flash[:notice] = @parser_feeds.map(&:parsed_job_message).join("\n")
              else
                flash[:notice] = "Please select feed url to process"
              end
            end
          end
        end
      end
    end
  end
end