#
# Credit; http://fernandomarcelo.com/2012/05/rails-admin-creating-a-custom-action/
#

require 'rails_admin/config/actions'
require 'rails_admin/config/actions/base'

module RailsAdminReporting
end

module RailsAdmin
  module Config
    module Actions
      class Reporting < RailsAdmin::Config::Actions::Base
        RailsAdmin::Config::Actions.register(self)
        # There are several options that you can set here.
        # Check https://github.com/sferik/rails_admin/blob/master/lib/rails_admin/config/actions/base.rb for more info.
        register_instance_option :collection do
          true
        end

        register_instance_option :member? do
          false
        end

        register_instance_option :http_methods do
          [:get]
        end

        register_instance_option :link_icon do
          'icon-asterisk icon-spin'
        end

        register_instance_option :controller do
          Proc.new do
            @counter = Exoteric.new(url: 'http://www.intensityjobs.com/')
          end
        end
      end
    end
  end
end