#
# Credit; http://fernandomarcelo.com/2012/05/rails-admin-creating-a-custom-action/
#

require 'rails_admin/config/actions'
require 'rails_admin/config/actions/base'

module RailsAdminFeedMapping
end

module RailsAdmin
  module Config
    module Actions
      class FeedMapping < RailsAdmin::Config::Actions::Base
        RailsAdmin::Config::Actions.register(self)
        # There are several options that you can set here.
        # Check https://github.com/sferik/rails_admin/blob/master/lib/rails_admin/config/actions/base.rb for more info.

        register_instance_option :member? do
          true
        end

        register_instance_option :http_methods do
          [:get, :put]
        end

        register_instance_option :link_icon do
           'icon-screenshot icon-spin'
        end

        register_instance_option :controller do
          Proc.new do
            if  request.get?
              render :action => @action.template_name
            elsif  request.put?
              if object.update_schema_mapping params[:feed_schema]
                flash[:notice] = "Schema saved successfully"
              else
                flash[:errors] = object.errors.full_message
              end
            end
          end
        end
      end
    end
  end
end