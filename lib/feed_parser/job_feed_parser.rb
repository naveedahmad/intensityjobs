class JobFeedParser
  attr_accessor :feed_url, :in_job_node, :current_job, :feed_schema, :new_job_ids, :duplicate_job_ids, :job_parser_feed

  def initialize(feed_url)
    @feed_url = feed_url
    @new_job_ids = []
    @duplicate_job_ids = []
  end

  def in_job_node?
    !!in_job_node
  end

  def feed_schema
    @feed_url.feed_schema
  end

  def feed_provider
    @feed_url.feed_provider
  end

  def create_feed_parser_logger
    @job_parser_feed = JobParserFeed.create feed_url_id: feed_url.id
  end

  def process_feed!
    raw_feed = feed_url.fetch_feed

    create_feed_parser_logger

    if raw_feed.is_a?(Hash)
      raw_feed.keys.each do |node_key|
        process_hash_elements(raw_feed[node_key], node_key)
      end
    elsif raw_feed.is_a?(Array)
      process_array_elements(raw_feed)
    end

    feed_parsing_finished
  end

  def feed_parsing_finished
    job_parser_feed.update_attributes new_job_ids: new_job_ids, duplicate_job_ids: duplicate_job_ids
    job_parser_feed.notify_parser_finished
    feed_url.update_attribute :last_run_at, DateTime.now
    job_parser_feed
  end

  def is_job_node?(node_key)
    feed_schema.job_node == node_key.to_s
  end

  def process_hash_elements(feed, key)
    feed.keys.each do |child_key|
      if feed[child_key].is_a?(Hash)
        process_hash_node(feed[child_key], child_key)
      elsif feed[child_key].is_a?(Array)
        process_array_node(feed[child_key], child_key)
      end
    end
  end

  def process_hash_node(feed, key)
    if is_job_node?(key)
      if feed_schema.job_node_wrapped_in_array?
        feed.each do |node|
          process_array_node feed, key
        end
      else
        process_job_hash feed
      end
    else
      if feed.is_a? Hash
        feed.keys.each do |key|
          process_hash_node(feed[key], key)
        end
      elsif feed.is_a? Array
        feed.each do |node|
          process_array_node node, key
        end
      end
    end
  end

  def process_array_node(feed, key)
    if is_job_node?(key)
      feed.each do |job_hash|
        process_job_hash job_hash
      end
    end
  end

  def process_job_hash(feed)
    job_node_started
    puts "jobs node  Feed: \n KEYS: #{feed.keys}"

    feed.keys.each do |key|
      if feed[key].is_a?(String)
        set_job_attr(key, feed[key])
      elsif feed[key].is_a?(Hash)
        process_job_node_nested_hash feed[key]
      end
    end
    job_node_finished
  end

  #TODO: DRY!
  def process_job_node_nested_hash(feed)

    feed.keys.each do |key|
      if feed[key].is_a?(String)
        set_job_attr(key, feed[key])
      elsif feed[key].is_a?(Hash)
        process_job_node_nested_hash feed[key]
      end
    end
  end

  def job_node_started
    @current_job = Job.new feed_url_id: feed_url.id, approved: !feed_url.approve_job_before_listing

    is_in_job_node = true
  end

  def job_node_finished
    if current_job.valid?
      if current_job.is_uniq_job?
        current_job.save
        new_job_ids << current_job.id
        puts "new job created! job id #{current_job.id} feed_url #{feed_url.id}"
      else
        duplicate_job_ids << current_job.get_matched_job.id
        puts "Duplicated job found, matched job# #{current_job.get_matched_job.id} \n JOB URL: #{current_job.url} \n DUPLICATE JOB URL: #{current_job.get_matched_job.url}\n feed_url#{feed_url.id}"
      end
    else
      @job_parser_feed.job_parser_notifications.create message: "Can't create Job  ERROR #{current_job.errors.full_messages}"
    end
    @current_job = nil
  end

  def set_job_attr(attr, value)
    mapped_job_attr = feed_schema.get_mapped_job_attr(attr)
    current_job.send "#{mapped_job_attr}=", value if mapped_job_attr && current_job
  end
end


