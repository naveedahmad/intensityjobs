class AddApproveToFeedUrls < ActiveRecord::Migration
  def change
    add_column :feed_urls, :approve_job_before_listing, :boolean, default: true
  end
end
