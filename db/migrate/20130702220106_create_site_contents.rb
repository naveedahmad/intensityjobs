class CreateSiteContents < ActiveRecord::Migration
  def change
    create_table :site_contents do |t|
      t.text :content
      t.boolean :visible

      t.timestamps
    end
  end
end
