class AddTrackingColumnToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :visit_count, :integer, default: 0
    add_column :jobs, :url_visit_count, :integer, default: 0
  end
end
