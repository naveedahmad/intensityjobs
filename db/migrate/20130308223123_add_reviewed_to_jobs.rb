class AddReviewedToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :approved, :boolean, default: false
    add_index :jobs, :approved
  end
end
