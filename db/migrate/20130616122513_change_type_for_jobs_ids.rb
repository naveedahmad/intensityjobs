class ChangeTypeForJobsIds < ActiveRecord::Migration
  def up
    change_column :job_parser_feeds, :new_job_ids, :text
    change_column :job_parser_feeds, :duplicate_job_ids, :text
  end

  def down
  end
end
