class CreateFeedProviders < ActiveRecord::Migration
  def change
    create_table :feed_providers do |t|
      t.string :name
      t.string :website
      t.string :description
      t.boolean :active
      t.integer :jobs_count
      t.integer :parsed_feeds_count
      t.boolean :is_processing_feed
      t.integer :processing_feed_url_id# will store FeedUrl which is currently being parsed

      t.timestamps
    end
    add_index :feed_providers, :active
    add_index :feed_providers, :is_processing_feed
  end
end
