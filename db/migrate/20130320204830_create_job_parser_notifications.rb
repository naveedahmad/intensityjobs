class CreateJobParserNotifications < ActiveRecord::Migration
  def change
    create_table :job_parser_notifications do |t|
      t.references :job_parser_feed
      t.text :message

      t.timestamps
    end
    add_index :job_parser_notifications, :job_parser_feed_id
  end
end
