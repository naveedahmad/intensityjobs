class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string :name
      t.integer :parent_location_id

      t.timestamps
    end
    add_index :locations, :parent_location_id
  end
end
