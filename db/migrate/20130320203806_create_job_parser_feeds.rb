class CreateJobParserFeeds < ActiveRecord::Migration
  def change
    create_table :job_parser_feeds do |t|
      t.references :feed_url
      t.string :new_job_ids
      t.string :duplicate_job_ids

      t.timestamps
    end
    add_index :job_parser_feeds, :feed_url_id
  end
end
