class AddJobCategoryToFeedUrl < ActiveRecord::Migration
  def change
    add_column :feed_urls, :job_category_id, :integer
    add_index :feed_urls, :job_category_id

    add_column :feed_urls, :location_id, :integer
    add_index :feed_urls, :location_id
  end
end
