class AddLastRunAtToFeedUrls < ActiveRecord::Migration
  def change
    add_column :feed_urls, :last_run_at, :datetime
  end
end
