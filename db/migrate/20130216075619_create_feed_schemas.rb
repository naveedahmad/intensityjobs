class CreateFeedSchemas < ActiveRecord::Migration
  def change
    create_table :feed_schemas do |t|
      t.string  :name
      t.string  :job_node
      t.boolean :job_node_wrapped_in_array
      t.text    :attributes_map

      t.timestamps
    end
  end
end
