class RenameFeedProviderToFeedUrlInJobs < ActiveRecord::Migration
  def up
    rename_column :jobs, :feed_provider_id, :feed_url_id
    add_index :jobs, :feed_url_id
  end

  def down
    rename_column :jobs, :feed_url_id, :feed_provider_id
    remove_index :jobs, :feed_provider_id
  end
end
