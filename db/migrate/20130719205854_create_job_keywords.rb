class CreateJobKeywords < ActiveRecord::Migration
  def change
    create_table :job_keywords do |t|
      t.references :job
      t.references :keyword

      t.timestamps
    end
    add_index :job_keywords, :job_id
    add_index :job_keywords, :keyword_id
  end
end
