class AddAttachmentRawFeedToJobParserFeeds < ActiveRecord::Migration
  def self.up
    change_table :job_parser_feeds do |t|
      t.attachment :raw_feed
    end
  end

  def self.down
    drop_attached_file :job_parser_feeds, :raw_feed
  end
end
