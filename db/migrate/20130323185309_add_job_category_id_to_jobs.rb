class AddJobCategoryIdToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :job_category_id, :integer
    add_index :jobs, :job_category_id

    add_column :jobs, :location_id, :integer
    add_index :jobs, :location_id
  end
end
