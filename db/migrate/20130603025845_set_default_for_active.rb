class SetDefaultForActive < ActiveRecord::Migration
  def up
    remove_column :jobs,:visible
    change_column_default :jobs, :active, true
    change_column_default :jobs, :approved, true
  end

  def down
  end
end
