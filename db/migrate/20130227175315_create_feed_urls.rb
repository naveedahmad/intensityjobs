class CreateFeedUrls < ActiveRecord::Migration
  def change
    create_table :feed_urls do |t|
      t.references :feed_schema
      t.references :feed_provider
      t.boolean :active
      t.string :url
      t.integer :feed_parsed_count
      t.integer :jobs_parsed_count
      t.boolean :processing_feed

      t.timestamps
    end
    add_index :feed_urls, :feed_schema_id
    add_index :feed_urls, :feed_provider_id
    add_index :feed_urls, :processing_feed
  end
end
