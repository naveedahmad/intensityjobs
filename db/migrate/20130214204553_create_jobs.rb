class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :identifier
      t.string :title
      t.string :token
      t.string :url
      t.text :description
      t.boolean :active
      t.boolean :visible #Admin can hide ACTIVE Job for whatever reason
      t.references :feed_provider
      t.integer :referenced_site_count

      t.timestamps
    end

    add_index :jobs, :active
    add_index :jobs, :visible
    add_index :jobs, :identifier
  end
end
