class AddFbTokenToAdmins < ActiveRecord::Migration
  def change
    add_column :admins, :fb_auth_token, :string
    add_column :admins, :token_expires_at, :datetime
  end
end
