class UpdateApprovedToFalse < ActiveRecord::Migration
  def up
    change_column_default :jobs, :approved, false
  end

  def down
  end
end
