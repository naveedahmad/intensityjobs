class CreateJobCategories < ActiveRecord::Migration
  def change
    create_table :job_categories do |t|
      t.string :name
      t.integer :parent_category_id

      t.timestamps
    end
    add_index :job_categories, :parent_category_id
  end
end
