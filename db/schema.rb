# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130815151403) do

  create_table "admins", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "fb_auth_token"
    t.datetime "token_expires_at"
  end

  add_index "admins", ["email"], :name => "index_admins_on_email", :unique => true
  add_index "admins", ["reset_password_token"], :name => "index_admins_on_reset_password_token", :unique => true

  create_table "feed_providers", :force => true do |t|
    t.string   "name"
    t.string   "website"
    t.string   "description"
    t.boolean  "active"
    t.integer  "jobs_count"
    t.integer  "parsed_feeds_count"
    t.boolean  "is_processing_feed"
    t.integer  "processing_feed_url_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
  end

  add_index "feed_providers", ["active"], :name => "index_feed_providers_on_active"
  add_index "feed_providers", ["is_processing_feed"], :name => "index_feed_providers_on_is_processing_feed"

  create_table "feed_schemas", :force => true do |t|
    t.string   "name"
    t.string   "job_node"
    t.boolean  "job_node_wrapped_in_array"
    t.text     "attributes_map"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "feed_urls", :force => true do |t|
    t.integer  "feed_schema_id"
    t.integer  "feed_provider_id"
    t.boolean  "active"
    t.string   "url"
    t.integer  "feed_parsed_count"
    t.integer  "jobs_parsed_count"
    t.boolean  "processing_feed"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.integer  "job_category_id"
    t.integer  "location_id"
    t.datetime "last_run_at"
    t.boolean  "approve_job_before_listing", :default => true
  end

  add_index "feed_urls", ["feed_provider_id"], :name => "index_feed_urls_on_feed_provider_id"
  add_index "feed_urls", ["feed_schema_id"], :name => "index_feed_urls_on_feed_schema_id"
  add_index "feed_urls", ["job_category_id"], :name => "index_feed_urls_on_job_category_id"
  add_index "feed_urls", ["location_id"], :name => "index_feed_urls_on_location_id"
  add_index "feed_urls", ["processing_feed"], :name => "index_feed_urls_on_processing_feed"

  create_table "job_categories", :force => true do |t|
    t.string   "name"
    t.integer  "parent_category_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "job_categories", ["parent_category_id"], :name => "index_job_categories_on_parent_category_id"

  create_table "job_keywords", :force => true do |t|
    t.integer  "job_id"
    t.integer  "keyword_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "job_keywords", ["job_id"], :name => "index_job_keywords_on_job_id"
  add_index "job_keywords", ["keyword_id"], :name => "index_job_keywords_on_keyword_id"

  create_table "job_parser_feeds", :force => true do |t|
    t.integer  "feed_url_id"
    t.text     "new_job_ids"
    t.text     "duplicate_job_ids"
    t.datetime "created_at",            :null => false
    t.datetime "updated_at",            :null => false
    t.string   "raw_feed_file_name"
    t.string   "raw_feed_content_type"
    t.integer  "raw_feed_file_size"
    t.datetime "raw_feed_updated_at"
  end

  add_index "job_parser_feeds", ["feed_url_id"], :name => "index_job_parser_feeds_on_feed_url_id"

  create_table "job_parser_notifications", :force => true do |t|
    t.integer  "job_parser_feed_id"
    t.text     "message"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "job_parser_notifications", ["job_parser_feed_id"], :name => "index_job_parser_notifications_on_job_parser_feed_id"

  create_table "jobs", :force => true do |t|
    t.string   "identifier"
    t.string   "title"
    t.string   "token"
    t.string   "url"
    t.text     "description"
    t.boolean  "active",                :default => true
    t.integer  "feed_url_id"
    t.integer  "referenced_site_count"
    t.datetime "created_at",                               :null => false
    t.datetime "updated_at",                               :null => false
    t.boolean  "approved",              :default => false
    t.integer  "job_category_id"
    t.integer  "location_id"
    t.integer  "visit_count",           :default => 0
    t.integer  "url_visit_count",       :default => 0
    t.string   "short_url"
    t.text     "short_description"
  end

  add_index "jobs", ["active"], :name => "index_jobs_on_active"
  add_index "jobs", ["approved"], :name => "index_jobs_on_approved"
  add_index "jobs", ["feed_url_id"], :name => "index_jobs_on_feed_url_id"
  add_index "jobs", ["identifier"], :name => "index_jobs_on_identifier"
  add_index "jobs", ["job_category_id"], :name => "index_jobs_on_job_category_id"
  add_index "jobs", ["location_id"], :name => "index_jobs_on_location_id"

  create_table "keywords", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "locations", :force => true do |t|
    t.string   "name"
    t.integer  "parent_location_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "locations", ["parent_location_id"], :name => "index_locations_on_parent_location_id"

  create_table "rails_admin_histories", :force => true do |t|
    t.text     "message"
    t.string   "username"
    t.integer  "item"
    t.string   "table"
    t.integer  "month",      :limit => 2
    t.integer  "year",       :limit => 8
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
  end

  add_index "rails_admin_histories", ["item", "table", "month", "year"], :name => "index_rails_admin_histories"

  create_table "reports", :force => true do |t|
    t.string   "body"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "site_contents", :force => true do |t|
    t.text     "content"
    t.boolean  "visible"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
