# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
IntensityJobs.FeedSchema =
  job_node_wrapped_in_array: false
  job_attributes_mapping:    {}
  job_attributes:            []
  job_node_name:             ''

  init: ->
    IntensityJobs.FeedSchema.bindAll()

  bindAll: ->
    IntensityJobs.FeedSchema.bindDeleteMatchingCallbacks()
    IntensityJobs.FeedSchema.initlizeFeedTabs()
    IntensityJobs.FeedSchema.bindAvailableUrlCallbacks()

    $("form#incoming_feed_configuration").on "ajax:success", (event, data, status, xhr) ->
      $("#feed_nodes").html data.feed_nodes
      $("#raw_feed pre").html data.raw_feed
      $("#json_feed").html data.json_feed

      IntensityJobs.FeedSchema.blindNodeCallbacks()
      IntensityJobs.FeedSchema.blindNodePopOverCallbacks()

  initlizeFeedTabs: ->
    $('#feed_tabs a').on 'click', (e) ->
      e.preventDefault()
      $(@).tab('show')

  blindNodeCallbacks: ->
    $(".string_node").on "click", ->
      if $(@).attr("pop") is undefined
        $(@).attr "pop", "shown"
        node_key = $(@).children(".node_key").html()
        $(@).popover { trigger: 'manual', placement: 'top', html : true, content: IntensityJobs.FeedSchema.contentForUnseselectAttributes(node_key) }

      $(@).popover('toggle')


  contentForUnseselectAttributes: (node_value) ->
    select_tag = $("<select class='schemaOptions'>").attr('id', node_value)
    $.each IntensityJobs.FeedSchema.job_attributes, (id, attr) ->
      select_tag.append IntensityJobs.FeedSchema.getSchemaSelectorOption(attr)

    link_tag =  $("<a class='done'>").html("Done").addClass("btn").attr('data-schema_selector', node_value)

    $("<div class='attr_map_popover'>").append(select_tag).append(link_tag)

  getSchemaSelectorOption: (jobAttr) ->
    new Option(jobAttr, jobAttr)

  setJobAttrValue: (job_attr, node_name) ->
    input_id = "feed_schema_"+job_attr+"_node_name"
    $("#feed_column_config input#"+input_id).val node_name

  clearJobAttrValue: (job_attr) ->
    input_id = "feed_schema_"+job_attr+"_node_name"

    $("input#"+input_id).val ''

  blindNodePopOverCallbacks: ->
    $('body').on 'click', '.popover a.done',(event) ->
      event.stopImmediatePropagation()
      schema_selector = $("select#"+$(@).data("schema_selector"))
      job_attr        = schema_selector.val()
      node_name       =  schema_selector.attr('id')
      IntensityJobs.FeedSchema.setJobAttrValue job_attr, node_name
      #TODO: hide popover
      false

  bindDeleteMatchingCallbacks: ->
    $("#feed_column_config a.delete").on 'click', (e) ->
      e.preventDefault()
      IntensityJobs.FeedSchema.clearJobAttrValue $(@).attr('job_attr')

  bindAvailableUrlCallbacks: ->
    $("#active_urls .feed_url").click (e)->
      e.preventDefault()
      $("input#feed_schema_feed_url").val $(@).html()
      $("input#feed_schema_feed_url_id").val $(@).data('url-id')
