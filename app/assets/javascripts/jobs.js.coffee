# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
IntensityJobs.Job =
  createSocialButton: ->
    $(".job-list").find(".pull-right ul.no-btn").socialLikes()
    $(".job-list").find(".pull-right").find("ul").removeClass('no-btn')

$ -> IntensityJobs.Job.createSocialButton()