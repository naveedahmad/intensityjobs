module JobsHelper
  def meta_keyword_for_job(job)

  end

  def jobs_title
    'Recent Jobs'
  end

  def class_for_category(category)
    if category == current_category
      'active category'
    else
      'category'
    end
  end

  def current_category
    @category || JobCategory.find_by_id(params[:job_category_id])
  end

  def show_ad_in_jobs?
    unless @jobs_count
      @jobs_count = 1
    else
      @jobs_count += 1
    end

    unless @ad_shown_for_this_request
      if @jobs_count > (rand(6) + 2)
        if @ad_shown_for_this_request
          false
        else
          @ad_shown_for_this_request = true
        end
      end
    else
      if @jobs_count > (rand(6) + 2)
        if @ad_shown_for_this_request
          false
        else
          @ad_shown_for_this_request = true
        end

      end
    end
  end
end

