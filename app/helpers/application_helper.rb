module ApplicationHelper
  def page_meta_description
    if @job
      @job.meta_description
    else
      content_for?(:meta_description) ? yield(:meta_description) : "IntensityJobs provides free access to job seekers to the millions of jobs from thousands of company websites and job boards. As the leading pay-for-performance recruitment advertising network, IntensityJob drives millions of targeted applicants to jobs in every field and is the most cost-effective source of candidates for thousands of companies."
    end
  end

  def page_meta_keywords
    keyword = ['jobs', 'work from home', 'career']
    rand = rand(3)
    first = keyword[rand]
    remaining_keyword = keyword.select{|key| key != first }.join(',')
    "#{first}, #{remaining_keyword}, intensity job, employment, new openings, recent jobs, job search engine, job listings, search jobs, job feed"
  end

  def page_meta_title
    if @job
      @job.title
    else
      content_for?(:meta_title) ? yield(:meta_title) : "Intensity jobs: HOT JOBS, new openings find your dream job and build your career. Employeer are hiring!"
    end
  end

  def page_meta_url
    "www.intensityjobs.com"
  end

  def spaghetti_2 name, path_or_items, options = {}
    output = ''.html_safe
    next_page = next_page_id(options)
    if path_or_items.class.name == 'String'
      output << "<div class=\"#{ name } spaghetti spaghetti-empty\" data-spaghetti-enabled=\"1\" data-spaghetti-url=\"#{ path_or_items }\" data-spaghetti-next-page=\"#{next_page}\"><div class=\"#{ name }-items\"></div><div class=\"throbber\">#{ image_tag 'throbber.gif' }</div></div>".html_safe
    else
      if path_or_items.any?
        if options[:partial]
          items = j(render(:partial => options[:partial], :collection => path_or_items))
        else
          items = j(render(path_or_items))
        end
        output << "$('.#{ name } .#{ name }-items').append('#{ items }');".html_safe
        output << "$('.#{ name }').data('spaghetti-next-page', #{ next_page });".html_safe
        output << "$('.#{ name }').data('spaghetti-enabled', '1');".html_safe
      else
        output << "$('.#{ name }').data('spaghetti-enabled', '0');".html_safe
        output << "$('.#{ name }').append($('<div class=\"spaghetti-disabled\"></div>'));".html_safe
      end
      output << "$('.#{ name }').removeClass('spaghetti-loading');".html_safe
    end
    output
  end

  protected
  def next_page_id(options = {})
    if params[:page]
      params[:page].to_i + 1
    else
      options[:starting_page] || 1
    end
  end
end
