module FeedSchemaHelper
  def xml_feed(json)
    content = ''

    if json.is_a?(String)
      content = json
    elsif  json.is_a?(Hash)
      json.keys.each do |sub_key|
        content += format_hash_key_with_value(json[sub_key], sub_key)
      end
    elsif json.is_a?(Array)
      content += format_array_key_with_value(sub_hash, '')
    end

    content.html_safe
  end


  def get_template_for_hash_node(hash, parent_key)
    content = content_tag :div, class: 'hashChilds', data: {parent: parent_key} do
      sub_content = ''
      hash.keys.each do |child_key|
        if hash[child_key].is_a?(String)
          sub_content += get_template_for_string(child_key, hash[child_key])
        elsif  hash[child_key].is_a?(Hash)
          sub_content += format_hash_key_with_value(hash[child_key], child_key)
        elsif hash[child_key].is_a?(Array)
          sub_content += format_array_key_with_value(hash[child_key], child_key)
        end
      end

      ("{"+ sub_content + "}").html_safe
    end

    content.html_safe
  end

  def get_template_for_array_elements(json, key)
    sub_content = ''
    json.each do |sub_hash|
      sub_content += get_template_for_array_node(sub_hash, key)
    end

    ("[" + sub_content + "]").html_safe
  end

  def get_template_for_array_node(json, parent_key)
    content = content_tag :div, class: 'arrayChild', data: { parent: parent_key } do
      if json.is_a?(Hash)
        format_hash_key_with_value(json, '')
      elsif json.is_a?(Array)
        get_template_for_array_elements(json, '')
      end
    end

    content.html_safe
  end

  def get_template_for_string(key, value)
   content_tag :div, class: 'string_node prettyprint alert', data: {key: key, title: get_title_for_string_node(key) } do
      close_tag = content_tag(:a, 'x', class: 'close', href: '#', data: { dismiss: 'alert'})
      (content_tag(:div, key, class: 'node node_key') + close_tag + content_tag(:div, raw(value), class: 'string_value')).html_safe
   end
  end

  def format_hash_key_with_value(json, key)
    content = get_template_for_hash_node(json, key)
    content_tag :div, class: 'hashNode hasIndentContent', data: {node: key} do
      node_name_div = ''
      if key.present?
        node_name_div = content_tag(:span, key, class: 'hash_node_name node') + ":"
      end
      (node_name_div + content_tag(:div, content, class: 'hashChild indentContnet')).html_safe
    end.html_safe
  end

  def format_array_key_with_value(json, key)
    content = get_template_for_array_elements(json, key)
    content_tag :div, class: 'arrayNode indentChild hasIndentContent', data: {node: key} do
      node_name_div = ''
      if key.present?
        node_name_div = content_tag(:span, key, class: 'array_node_name node') + ":"
      end
      node_name_div + content_tag(:div, content, class: 'arrayChild indentContent')
    end.html_safe
  end

  def get_title_for_string_node(key)
    "Map Job column with Feed node <b>#{key}</b>".html_safe
  end
end

