# == Schema Information
#
# Table name: reports
#
#  id         :integer          not null, primary key
#  body       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

module Reporting
  class Report < ActiveRecord::Base
    attr_accessible :body
  end
end
