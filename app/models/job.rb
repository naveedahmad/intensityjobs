# == Schema Information
#
# Table name: jobs
#
#  id                    :integer          not null, primary key
#  identifier            :string(255)
#  title                 :string(255)
#  token                 :string(255)
#  url                   :string(255)
#  description           :text
#  active                :boolean          default(TRUE)
#  feed_url_id           :integer
#  referenced_site_count :integer
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  approved              :boolean          default(FALSE)
#  job_category_id       :integer
#  location_id           :integer
#  visit_count           :integer          default(0)
#  url_visit_count       :integer          default(0)
#  short_url             :string(255)
#

class Job < ActiveRecord::Base
  attr_accessible :description, :title, :url, :identifier, :job_category_id, :feed_url_id, :location_id, :approved, :active, :expired, :short_description, :short_url

  validates :title, :description, :url, presence: true
  validates :url, uniqueness: true


  before_create :assign_token
  before_create :ensure_identifier
  after_create :generate_short_description_and_meta_info

  belongs_to :feed_url
  belongs_to :job_category
  belongs_to :location
  has_many :job_keywords
  has_many :keywords, through: :job_keywords


  class << self
    def next_page
      current_page + 1
    end

    def newest
      order 'updated_at DESC'
    end

    def for_sitemap(days_ago=2)
      active_and_approved.newest.where("created_at > ?", days_ago.days.ago)
    end

    def active_and_approved
      where active: true, approved: true
    end


    def feed_attributes
      %w[description title url identifier] # column_names #-  blacklist_attributes
    end

    def blacklist_attributes
      %w[id created_at updated_at visible token active feed_provider_id referenced_site_count]
    end

    def filter(params)
      limit 10
    end

    def fetch(_job_id)
      if /(\d)+-\w*/ =~ _job_id
        job_id, job_title = _job_id.to_s.split('-')
      else
        job_id = _job_id.to_i
        job_title = _job_id.to_s
      end
      job = Job.find_by_id(job_id) || Job.find_by_title(job_title)

      raise ActiveRecord::RecordNotFound if job.nil?

      job.increment_visit
      job
    end

    def track(job_token)
      job = Job.find(job_token)
      job.incremet_url_visit

      job
    end

  end

  def meta_description
      short_description || "IntensityJobs provides free access to job seekers to the millions of jobs from thousands of company websites and job boards. As the leading pay-for-performance recruitment advertising network, IntensityJob drives millions of targeted applicants to jobs in every field and is the most cost-effective source of candidates for thousands of companies."
  end

  def meta_keyword

  end

  def to_param
    "#{id}-#{title.to_s.truncate(100)}"
  end

  def is_uniq_job?
    get_matched_job.blank?
  end

  def get_matched_job
    Job.find_by_identifier(ensure_identifier)
  end

  def increment_visit
    update_attribute :visit_count, visit_count.to_i + 1
  end

  def incremet_url_visit
    update_attribute :url_visit_count, url_visit_count.to_i + 1
  end

  def share_job(social_sites = "twitter,facebook,linkedin")
    social_sites.to_s.split(",").each do |site|
      case site
        when "twitter"
          share_on_twitter
        when "facebook"
          share_on_facebook
        when "linkedin"
          share_on_linkedin
      end
    end

    def share_on_linkedin

    end

    def share_on_facebook

    end

    def share_on_twitter
      Twitter.update("I'm tweeting with @gem!")
    end
  end

  protected
  def assign_token
    self.token = Utils::TokenGenerator.get_uniq_token Job
  end

  def assign_token!
    assign_token
    save!
  end

  def ensure_identifier
    if identifier.blank?
      #TODO: lets think through on how to avoid duplicate jobs in identifier is not configured in schema
      if same_job = Job.find_by_url(self.url)
        self.identifier = same_job.url
      else
        self.identifier = self.url
      end
    end
  end

  def generate_short_description_and_meta_info
    sanitize_des = description.to_s.gsub(/<img[^>]+>/, '')

    if sanitize_des.length < 300
      self.short_description = sanitize_des
    else
      i = sanitize_des.index(/<\/(div|span|p|)>/)
      index = i.to_i
      while i && i < 300 do
        i = sanitize_des.index(/<\/(div|span|p|)>/, i+1)
        index= i.to_i
      end
      if i
        self.short_description = sanitize_des[0..i-1]
      else
        self.short_description = sanitize_des.truncate(300)
      end

    end
    save validate: false
  end
end
