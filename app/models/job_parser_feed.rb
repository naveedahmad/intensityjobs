# == Schema Information
#
# Table name: job_parser_feeds
#
#  id                    :integer          not null, primary key
#  feed_url_id           :integer
#  new_job_ids           :text
#  duplicate_job_ids     :text
#  created_at            :datetime         not null
#  updated_at            :datetime         not null
#  raw_feed_file_name    :string(255)
#  raw_feed_content_type :string(255)
#  raw_feed_file_size    :integer
#  raw_feed_updated_at   :datetime
#

class JobParserFeed < ActiveRecord::Base
  serialize :duplicate_job_ids
  serialize :new_job_ids

  belongs_to :feed_url
  has_many :job_parser_notifications, inverse_of: :job_parser_feed, dependent: :destroy

  after_create :upload_raw_feed_file

  attr_accessible :duplicate_job_ids, :new_job_ids, :feed_url_id
  has_attached_file :raw_feed

  def parsed_job_message
    "parserJob# #{self.id} FeedUrl#{feed_url.url}: Found #{new_job_ids.size} new and #{duplicate_job_ids.size} duplicated job\n"
  end

  def notify_parser_finished
    JobParserNotifier.notify_parser_job_completed(self).deliver
  end
  private

  def upload_raw_feed_file
    feed = feed_url.raw_feed

    if feed.present?
      feed_filename = "/tmp/feed_job#{self.id}"
      File.open feed_filename, "w" do |f|
        f.puts feed.inspect
      end

      self.raw_feed = open(feed_filename)

      save
    end
  end
end
