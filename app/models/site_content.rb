# == Schema Information
#
# Table name: site_contents
#
#  id         :integer          not null, primary key
#  content    :text
#  visible    :boolean
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class SiteContent < ActiveRecord::Base
  attr_accessible :content, :visible
  after_save :clear_cache_content

  def clear_cache_content
    ActionController::Base.new.expire_fragment "SEO_LINKS"
  end

  class << self
    def visible
      where visible: true
    end
  end
end
