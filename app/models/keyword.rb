class Keyword < ActiveRecord::Base
  attr_accessible :name

  has_many :job_keywords
  has_many :jobs, through: :job_keywords
end
