# == Schema Information
#
# Table name: feed_schemas
#
#  id                        :integer          not null, primary key
#  name                      :string(255)
#  job_node                  :string(255)
#  job_node_wrapped_in_array :boolean
#  attributes_map            :text
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#

class FeedSchema < ActiveRecord::Base
  serialize :attributes_map, Hash

  #TODO: may be we can move this to some module
  class << self
    def accessor_for(node_name)
      "#{node_name}_node_name"
    end
  end

  attr_accessor *Job.feed_attributes.map { |attr| FeedSchema.accessor_for attr }
  attr_accessor :feed_url, :feed_url_id
  attr_accessible *Job.feed_attributes.map { |attr| FeedSchema.accessor_for attr }
  attr_accessible :attributes_map, :job_node, :job_node_wrapped_in_array, :name, :feed_urls_attributes, :feed_url

  has_many :feed_urls

  #validates :name, presence: true

  after_initialize :set_attribute_map_nodes

  accepts_nested_attributes_for :feed_urls

  def get_mapped_feed_node(attr)
    if self.attributes_map
      attributes_map.key attr
    end
  end

  def get_mapped_job_attr(feed_node_name)
    if attributes_map
      self.attributes_map[feed_node_name]
    end
  end

  def update_schema_mapping(params)
    self.attributes = params
    setup_attributes_map
    save
  end

  protected
  def setup_attributes_map
    self.attributes_map = {}

    Job.feed_attributes.each do |attr|
      feed_node_name = send FeedSchema.accessor_for(attr)
      self.attributes_map[feed_node_name] = attr.to_s if feed_node_name.present?
    end
  end

  def set_attribute_map_nodes
    if self.attributes_map.present?
      Job.feed_attributes.each do |attr|
        send "#{FeedSchema.accessor_for(attr)}=", get_mapped_feed_node(attr.to_s)
      end
    end
  end

end
