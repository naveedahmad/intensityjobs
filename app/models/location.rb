# == Schema Information
#
# Table name: locations
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  parent_location_id :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class Location < ActiveRecord::Base
  attr_accessible :name, :parent_location_id, :job_ids

  belongs_to :parent_location, class_name: 'Location'
  has_many :sub_locations,     class_name: 'Location', foreign_key: :parent_location_id
  has_many :jobs

  validates :name, presence: true
end
