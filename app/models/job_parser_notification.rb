# == Schema Information
#
# Table name: job_parser_notifications
#
#  id                 :integer          not null, primary key
#  job_parser_feed_id :integer
#  message            :text
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class JobParserNotification < ActiveRecord::Base
  attr_accessible :message

  belongs_to :job_parser_feed, inverse_of: :job_parser_notifications

end
