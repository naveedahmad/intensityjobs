# == Schema Information
#
# Table name: feed_urls
#
#  id                :integer          not null, primary key
#  feed_schema_id    :integer
#  feed_provider_id  :integer
#  active            :boolean
#  url               :string(255)
#  feed_parsed_count :integer
#  jobs_parsed_count :integer
#  processing_feed   :boolean
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  job_category_id   :integer
#  location_id       :integer
#  last_run_at       :datetime
#

class FeedUrl < ActiveRecord::Base
  attr_accessible :active, :url, :feed_schema_id, :feed_provider_id, :approve_job_before_listing

  belongs_to :feed_schema
  belongs_to :feed_provider
  has_many :jobs

  validates :url, presence: true, uniqueness: true

  class << self
    def active
      where active: true
    end
  end

  def ran_today?
    last_run_at && last_run_at.to_date.today?
  end

  def feed_parsing_started
     update_attribute :processing_feed, true
  end

  def feed_parsing_finished
    update_attribute :processing_feed, false
  end

  def name
    "#{id}- #{url}"
  end

  def raw_feed
=begin
    if Rails.env.development?
      filename ="github.atom" #rand(3) % 2 == 1 ? "37sig.rss" : "github.atom"
      xmp_feed = File.open("tmp/feed/#{filename}").read
    else
=end

      unless @raw_feed
        if url.present?
          @raw_feed = Mechanize.new.get(url).body
        end
      end

      @raw_feed
    #end

  end

  #accepts_nested_attributes_for :feed_provider
  def fetch_feed
    if raw_feed.present?
      Crack::XML.parse(raw_feed)
    else
      ""
    end
  end
end
