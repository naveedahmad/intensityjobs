# == Schema Information
#
# Table name: job_categories
#
#  id                 :integer          not null, primary key
#  name               :string(255)
#  parent_category_id :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#

class JobCategory < ActiveRecord::Base
  attr_accessible :name, :parent_category_id, :job_ids

  has_many :jobs
  has_many :sub_categories, class_name: "JobCategory", foreign_key: :parent_category_id
  belongs_to :parent_category, class_name: "JobCategory"

  validates :name, presence: true

  class << self
    def root_categories
      where parent_category_id: nil
    end
  end
end
