# == Schema Information
#
# Table name: feed_providers
#
#  id                     :integer          not null, primary key
#  name                   :string(255)
#  website                :string(255)
#  description            :string(255)
#  active                 :boolean
#  jobs_count             :integer
#  parsed_feeds_count     :integer
#  is_processing_feed     :boolean
#  processing_feed_url_id :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

class FeedProvider < ActiveRecord::Base
  attr_accessible :active, :description, :is_processing_feed,
                  :last_feed_processed_at, :name, :total_feed_processed,
                  :website, :feed_urls_attributes
  has_many :feed_urls
  has_many :jobs, through: :feed_urls

  accepts_nested_attributes_for :feed_urls

end
