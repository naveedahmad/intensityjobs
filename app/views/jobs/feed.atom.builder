atom_feed('xmlns:app' => 'http://www.w3.org/2007/app',
          'xmlns:openSearch' => 'http://a9.com/-/spec/opensearch/1.1/') do |feed|
  feed.title jobs_title
  feed.updated @jobs.first.created_at
  #feed.tag!({openSearch: @jobs.size}, 10)


  @jobs.each do |job|
    feed.entry(job, url: job_path(job)) do |entry|
      entry.title job.title, type: 'html'
      entry.content type: 'html' do
        entry.cdata! job.description
      end
      entry.tag! 'app:edited', job.updated_at.strftime("%Y-%m-%dT%H:%M:%SZ")
      entry.pubDate job.created_at.to_s(:rfc822)
      entry.link job_url(job)
      entry.guid job_url(job)
    end
  end
end
