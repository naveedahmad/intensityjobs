class JobParserNotifier < ActionMailer::Base
  default from: "naveedahmada036@gmail.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.job_parser_notifier.notify_parser_job_completed.subject
  #
  def notify_parser_job_completed(feed_job)
    @feed_job = feed_job

    mail to: "naveedahmada036@gmail.com", subject: "Job parsing finished MESSAGE: #{feed_job.parsed_job_message}"
  end
end
