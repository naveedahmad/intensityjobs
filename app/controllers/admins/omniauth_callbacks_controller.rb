class Admins::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    current_admin.set_fb_auth request.env["omniauth.auth"]
    redirect_to 'leads/reporting~report/reporting', notice: "Facebook token saved successfully."
  end
end
