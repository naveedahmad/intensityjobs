class HomeController < ApplicationController
  def sitemap
    data =  open("http://s3.amazonaws.com/intensityjobs/sitemaps/sitemap.xml.gz")
    send_data data.read, filename: "sitemap.xml.gz", type: "application/x-gzip; charset=binary", stream: 'true', buffer_size: '4096'

  end
end
