class FeedSchemasController < ApplicationController
  def fetch_sample
     #filename = rand(3) % 2 == 1 ? "37sig.rss" : "github.atom"
     #client= Mechanize.new  
     #xmp_feed=     client.get(params[ "feed_schema"]['feed_url']).body
     feed_url = FeedUrl.find (params[ "feed_schema"]['feed_url_id'])
     #xml_feed =  File.open("tmp/feed/#{filename}").read
     @xml = feed_url.fetch_feed

    feed_node_content = render_to_string file: "/feed_schemas/fetch_sample.slim", layout: false
    render json: {feed_nodes: feed_node_content, raw_feed: feed_url.raw_feed, json_feed: @xml.to_s }
  end
end
