class JobsController < ApplicationController
  # GET /jobs
  # GET /jobs.json
  def index
    @jobs = fetch_filtered_jobs(decorate: true, paginate: true)

    respond_to do |format|
      format.html # index.html.slim
      format.js # index.js.slim
      format.json { render json: @jobs }
    end
  end

  def feed
    @jobs = fetch_filtered_jobs.limit(50)
  end
  # GET /jobs/1
  # GET /jobs/1.json
  def show
    @job = Job.fetch(params[:id]).decorate

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @job }
    end
  end

  def apply
    job = Job.track(params[:id])
    
    respond_to do |format|
      format.html { redirect_to job.url }
      format.json { render json: job }
    end
  end

  # GET /jobs/new
  # GET /jobs/new.json
  def new
    @job = Job.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @job }
    end
  end

  # GET /jobs/1/edit
  def edit
    @job = Job.find(params[:id])
  end

  # POST /jobs
  # POST /jobs.json
  def create
    @job = Job.new(params[:job])

    respond_to do |format|
      if @job.save
        format.html { redirect_to @job, notice: 'Job was successfully created.' }
        format.json { render json: @job, status: :created, location: @job }
      else
        format.html { render action: "new" }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /jobs/1
  # PUT /jobs/1.json
  def update
    @job = Job.find(params[:id])

    respond_to do |format|
      if @job.update_attributes(params[:job])
        format.html { redirect_to @job, notice: 'Job was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jobs/1
  # DELETE /jobs/1.json
  def destroy
    @job = Job.find(params[:id])
    @job.destroy

    respond_to do |format|
      format.html { redirect_to jobs_url }
      format.json { head :no_content }
    end
  end

  protected
  def fetch_filtered_jobs(options = {})
    options.with_defaults(decorate: false, paginate: false)

    jobs = Job.active_and_approved.newest
    jobs = jobs.page(params[:page]).per(15) if options[:paginate]
    jobs = jobs.decorate if options[:decorate]

    jobs
  end
end
